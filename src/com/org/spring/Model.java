package com.org.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Model {
	@Value(value="think pad")
	private String name;
	@Value(value="black")
	private String color;
	@Value(value="500gb hardisk,6gbrom,icore processor")
	private String Specification;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSpecification() {
		return Specification;
	}
	public void setSpecification(String specification) {
		Specification = specification;
	}
	@Override
	public String toString() {
		return "Model [name=" + name + ", color=" + color + ", Specification=" + Specification + "]";
	}
	

}
