package com.org.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Laptop {
	
	@Value(value = "apple")
	private String name;

	@Value(value = "10000.500")
	private Double price;
	
	@Value(value = "ios")
	private String osType;
	
	@Autowired
	private Model model;

	
public Laptop() {
System.out.println(this.getClass().getSimpleName()+"object created");
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public Double getPrice() {
	return price;
}


public void setPrice(Double price) {
	this.price = price;
}


public String getOsType() {
	return osType;
}


public void setOsType(String osType) {
	this.osType = osType;
}


public Model getModel() {
	return model;
}


public void setModel(Model model) {
	this.model = model;
}


@Override
public String toString() {
	return "Laptop [name=" + name + ", price=" + price + ", osType=" + osType + ", model=" + model + "]";
}
}
