package com.org.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("spring-web.xml");
		Laptop bean = context.getBean(Laptop.class);
		System.out.println(bean);
	}

}
